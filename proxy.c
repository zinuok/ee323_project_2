/* proxy.c
** Name : Jeon jinwoo
** Project #2
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <assert.h>

#define TRUE 1             // Boolean
#define BACKLOG 10         // how many pending connections queue will hold
#define MAXDATASIZE 100000 // max number of bytes we can get at once

// mode for using 'validate_HTTP_request' function.
enum get_mode
{
  URL = 1,
  Host,
  Port,
  Path,
};

void sigchld_handler(int s)
{
  while (waitpid(-1, NULL, WNOHANG) > 0)
    ;
}

// get sockaddr, IPv4 or IPv6:
void *
get_in_addr(struct sockaddr *sa)
{
  if (sa->sa_family == AF_INET)
    return &(((struct sockaddr_in *)sa)->sin_addr);

  return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

// validate HTTP request message format. check following:
// 1) does [method:GET], [URL], [version:HTTP/1.0] exist in request line?
// 2) does "Host" HTTP header exist in header lines?
// return: 1 on vaild msg, 0 on invalid msg(400 Bad Request cases)
int validate_HTTP_request(char *msg)
{
  char *method[8] = {"GET", "CONNECT", "HEAD", "OPTIONS", "POST", "PUT", "DELETE", "TRACE"};
  // 1) request line validation
  int is_method = 0;

  // method validation
  for (int i = 0; i < 8; i++)
  {
    if (strstr(msg, method[i]) != NULL)
    {
      is_method = 1;
      break;
    }
  }
  if (!is_method)
    return 0;
  // URL exist validation
  if (strstr(msg, "http://") == NULL)
    return 0;

  // http version validation
  if (strstr(msg, "HTTP/1.0") == NULL)
    return 0;

  // 2) header line validation
  if (strstr(msg, "Host:") == NULL)
    return 0;

  return 1;
}

// get proper information piece, according to mode
// >> mode: get URI(1), get host(2), get port number(3), get path(4)
// return: 1 on success, 0 on any failure
int parsing_HTTP_request(char *msg, char *dest_buf, int mode)
{
  char *ptr = NULL;
  char *backup = NULL; // backup of ptr
  char cpy_msg[MAXDATASIZE]; // copy input msg to this, since we cut and make token for that
  char *method[8] = {"GET", "CONNECTION", "HEAD", "OPTIONS", "POST", "PUT", "DELETE", "TRACE"};
  memset(cpy_msg, 0, MAXDATASIZE);
  memcpy(cpy_msg, msg, strlen(msg) + 1);

  switch (mode)
  {
  case URL:
    for (int i = 0; i < 8; i++)
    {
      ptr = strstr(cpy_msg, method[i]);
      if (ptr != NULL)
        break;
    }
    //ptr = strstr(cpy_msg, "GET");
    ptr = strtok(ptr, " ");
    ptr = strtok(NULL, " ");

    memcpy(dest_buf, ptr, strlen(ptr) + 1);
    dest_buf[strlen(ptr)] = '\0';
    break;

  case Host:
    ptr = strstr(cpy_msg, "Host");
    ptr = strtok(ptr, " ");
    ptr = strtok(NULL, " ");
    strtok(NULL, "\r\n");

    // strip CRLF at end
    backup = ptr;
    while (*ptr != 13)
    { // 13: CR
      ptr++;
    }
    *ptr = '\0';
    memcpy(dest_buf, backup, strlen(backup) + 1);
    break;

  case Port:
    for (int i = 0; i < 8; i++)
    {
      ptr = strstr(cpy_msg, method[i]);
      if (ptr != NULL)
        break;
    }
    ptr = strtok(ptr, " ");
    ptr = strtok(NULL, " ");

    char *protocol = strstr(ptr, "://"); // position of protocol sign
    if (protocol != NULL)
      ptr = protocol + 3;
    char *port = strchr(ptr, ':');
    if (port == NULL)
    { // port doesn't exist=>use 80
      char *port_80 = "80";
      memcpy(dest_buf, port_80, strlen(port_80) + 1);
      dest_buf[strlen(port_80)] = '\0';
      break;
    }

    port++;

    port = strtok(port, "/");
    memcpy(dest_buf, port, strlen(port) + 1);
    dest_buf[strlen(port)] = '\0';
    break;

  case Path:
    break;

  default:
    break;
  }

  return 1;
}

/*
 * find control sequence from each line
 * return flag:
 * flag = -1 => only single '\r\n' is in buffer
 * flag = 1 => buffer is end with '\r\n'
 * flag = 2 => now become '\r\n' twice
 * flag = 0 => buffer isn't end with '\r\n'
 */
int find_control_seq(char *buf, int was_ENT)
{
  assert(buf != NULL);
  char *last_char = NULL;
  size_t buf_len = strlen(buf);
  assert(buf_len > 0);

  last_char = buf + (buf_len - 2);
  if ((was_ENT == 1 || was_ENT == -1) && strcmp(buf, "\r\n") == 0) // '\r\n' twice ?
    return 2;

  if (strcmp(last_char, "\r\n") == 0)
  {
    if (buf_len == 2)
      return -1;
    else
      return 1;
  }

  return 0;
}


// check for blacklist url should be blocked or not
// return 1 if blacklist.txt is given and URL is included in the file. return 0 else
int 
check_blacklist(char *url)
{
  int not_blacklist = isatty(0); // 0 if blacklist is redirected to input, otherwise 1 

  if (!not_blacklist) { // blacklist is given as redirected input
    char black_site[MAXDATASIZE];
    memset(black_site, 0, MAXDATASIZE);
    while (fgets(black_site, MAXDATASIZE, stdin) != NULL)
    {
      // get each site in a line
      char *new_line = NULL;
      new_line = strchr(black_site, '\n');
      if (new_line != NULL)
        *new_line = '\0';

      // compare with given url
      if (strcmp(black_site, url) == 0)
        return 1;

      // clear buffer
      memset(black_site, 0, MAXDATASIZE);
    }
  }

  // don't have to block this URL
  return 0;
}

int main(int argc, char *argv[])
{
  int sockfd, new_fd; // listen on sock_fd, new connection on new_fd (for client)
  int numbytes;       // received bytes from client
  struct addrinfo hints, *servinfo, *p;
  struct sockaddr_storage their_addr; // connector's address information
  socklen_t sin_size;
  struct sigaction sa;
  int yes = 1;
  char s[INET6_ADDRSTRLEN];
  char buf[MAXDATASIZE]; // buffer: store total HTTP request message from client
  int rv;
  char *port = NULL; // port number for proxy


  if (argc != 2)
  {
    if (argc == 1)
      fprintf(stderr, "Port number is not specified.\n");
    else
      fprintf(stderr, "Number of arguments is not appropriate.\n");
    exit(1);
  }

  // 0) get port number from stdin and setting
  port = strdup(argv[1]);

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE; // use my IP
  if ((rv = getaddrinfo(NULL, port, &hints, &servinfo)) != 0)
  {
    //fprintf(stderr, "proxy: getaddrinfo: %s\n", gai_strerror(rv));
    return 1;
  }

  // 1) socket & bind: loop through all the results and bind to the first we can
  for (p = servinfo; p != NULL; p = p->ai_next)
  {
    if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
    {
      //fprintf(stderr, "server: trying to find socket\n");
      continue;
    }
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
    {
      //fprintf(stderr, "server: failed to set sockopt\n");
      exit(1);
    }
    if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1)
    {
      close(sockfd);
      //fprintf(stderr, "server: trying to bind\n");
      continue;
    }
    break;
  }
  if (p == NULL)
  {
    fprintf(stderr, "proxy: failed to bind.  Address maybe already in use. trying again\n");
    return 2;
  }
  freeaddrinfo(servinfo); // all done with this structure
  if (listen(sockfd, BACKLOG) == -1)
  {
    fprintf(stderr, "proxy: failed to listen\n");
    exit(1);
  }

  sa.sa_handler = sigchld_handler; // reap all dead processes
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART;
  if (sigaction(SIGCHLD, &sa, NULL) == -1)
  {
    fprintf(stderr, "proxy: failed to sigaction\n");
    exit(1);
  }

  // 2) accept:
  while (TRUE)
  {
    sin_size = sizeof their_addr;
    new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);

    if (new_fd == -1)
    {
      //fprintf(stderr, "server: trying to accept\n");
      continue;
    }
    inet_ntop(their_addr.ss_family,
              get_in_addr((struct sockaddr *)&their_addr),
              s, sizeof s);

    // 3) receive messages from clients
    memset(buf, 0, MAXDATASIZE);
    pid_t pid = fork();
    if (!pid)
    {                // this is the child process
      close(sockfd); // child doesn't need the listener
      // parsed contents: url, host, port, path of HTTP request message from client
      char get_URL[MAXDATASIZE];
      char get_host[MAXDATASIZE];
      char get_port[MAXDATASIZE];
      char get_path[MAXDATASIZE];
      memset(get_URL, 0, MAXDATASIZE);
      memset(get_host, 0, MAXDATASIZE);
      memset(get_port, 0, MAXDATASIZE);
      memset(get_path, 0, MAXDATASIZE);

      // A. get initial HTTP request from client and validate HTTP request message
      int read_bytes = 0;  // read bytes from client at once
      int total_bytes = 0; // total bytes from client request
      int is_ENT = 0;      // if 'ENTER' is at end of a line.
      char tmp_buf[MAXDATASIZE]; // store received content temporarily

      int one_string = 0;  // whether get input string as only one-string or cover multiple-string
      if (one_string == 1) // ONE-STRING Version (it's just for debugging)
      {
        read_bytes = recv(new_fd, buf, MAXDATASIZE, 0);
        if (read_bytes == -1)
          fprintf(stderr, "failed to receive request message\n");
      }

      else // ONE-STRING+MULTI-STRING Version
      {
        while (TRUE)
        {
          memset(tmp_buf, 0, MAXDATASIZE);
          read_bytes = recv(new_fd, tmp_buf, MAXDATASIZE, 0);

          if (read_bytes == -1)
          {
            fprintf(stderr, "failed to receive request message\n");
            break;
          }
          else if (read_bytes == 0)
          {
            break;
          }

          total_bytes = total_bytes + read_bytes;

          if (strstr(tmp_buf, "\r\n\r\n") != NULL)
          {
            memcpy(buf, tmp_buf, strlen(tmp_buf));
            break;
          }

          strcat(buf, tmp_buf);

          is_ENT = find_control_seq(tmp_buf, is_ENT);
          if (is_ENT == 2)
          { // if control sequence, exit.
            break;
          }
        }

        numbytes = total_bytes;

        if (numbytes == -1)
        {
          fprintf(stderr, "proxy: failed to receive from client\n");
          exit(1);
        }
        else if (numbytes == 0)
        { // client is finished
          printf("proxy: received 0-bytes from client. finishing\n");
          close(new_fd);
          exit(1);
        }
      }
      buf[total_bytes] = '\0';

      if (!validate_HTTP_request(buf))
      {
        printf("HTTP/1.0 400 Bad Request \n");
        close(new_fd);
        exit(1);
      }

      // B. parsing the HTTP request msg => extracting host,port,path info
      parsing_HTTP_request(buf, get_URL, URL);
      parsing_HTTP_request(buf, get_host, Host);
      parsing_HTTP_request(buf, get_port, Port);
      parsing_HTTP_request(buf, get_path, Path);


      // C. connect the host by extracted URL.
      int sockfd_http; // listen on sock_fd
      struct addrinfo hints_http, *servinfo_http, *p_http;
      char s_http[INET6_ADDRSTRLEN];
      int rv_http;

      memset(&hints_http, 0, sizeof hints_http);
      hints_http.ai_family = AF_UNSPEC;
      hints_http.ai_socktype = SOCK_STREAM;

      // check whether URL in received request should be blocked or not
      int should_block = check_blacklist(get_URL); // should block this URL?

      if (should_block){ // shold be blocked. set www.warning.or.kr
        if ((rv_http = getaddrinfo("www.warning.or.kr", "80", &hints_http, &servinfo_http)) != 0){
          fprintf(stderr, "proxy: getaddrinfo: %s\n", gai_strerror(rv_http));
          return 1;
        }
      }
      else {
        if ((rv_http = getaddrinfo(get_host, get_port, &hints_http, &servinfo_http)) != 0){
          fprintf(stderr, "proxy: getaddrinfo: %s\n", gai_strerror(rv_http));
          return 1;
        }
      }

      for (p_http = servinfo_http; p_http != NULL; p_http = p_http->ai_next)
      {
        // 1) socket: loop through all the results and connect to the first we can
        if ((sockfd_http = socket(p_http->ai_family, p_http->ai_socktype, p_http->ai_protocol)) == -1)
          continue;

        // 2) connect:
        if (connect(sockfd_http, p_http->ai_addr, p_http->ai_addrlen) == -1)
        {
          close(sockfd_http);
          continue;
        }

        break;
      }

      if (p_http == NULL)
      {
        fprintf(stderr, "proxy: failed to connect.\n");
        return 2;
      }

      inet_ntop(p_http->ai_family, get_in_addr((struct sockaddr *)p_http->ai_addr), s_http, sizeof s_http);
      freeaddrinfo(servinfo_http); // all done with this structure

      // D. request HTTP msg(from client) to server for proper file
      if (should_block){
        char warning_request[MAXDATASIZE] = "GET http://www.warning.or.kr HTTP/1.0\r\nHost: www.warning.or.kr\r\n\r\n";
        if (send(sockfd_http, warning_request, strlen(warning_request), 0) == -1)
          fprintf(stderr, "failed to send request message to server\n");
      }
      else {
        if (send(sockfd_http, buf, strlen(buf), 0) == -1)
          fprintf(stderr, "failed to send request message to server\n");
      }
 

      int respond_bytes; // bytes of response from server at once
      int total_respond_bytes = 0; // total bytes of response from server
      char received_buf[MAXDATASIZE]; // buffer: store messages from client
      memset(received_buf, 0, MAXDATASIZE);

      while (TRUE)
      {
        // receive respond from server
        respond_bytes = recv(sockfd_http, received_buf, MAXDATASIZE, 0);

        if (respond_bytes == -1)
        {
          fprintf(stderr, "failed to receive respond message\n");
          break;
        }
        else if (respond_bytes == 0)
          break;
        total_respond_bytes += respond_bytes;

        send(new_fd, received_buf, respond_bytes, 0); // send respond to clinet
        memset(received_buf, 0, MAXDATASIZE);
      }

      // close connection to server
      close(sockfd_http);
    } // client is finished

    close(new_fd); // parent doesn't need this
  }
  return 0;
}