#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include <netinet/tcp.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <assert.h>

int socket_connect(char *host, in_port_t port){
	struct hostent *hp;
	struct sockaddr_in addr;
	int on = 1, sock;     
    
    // 1) 호스트네임(URL)에 대응하는 hostent 구조체 반환
	if((hp = gethostbyname(host)) == NULL){
		herror("gethostbyname");
		exit(1);
	}

    // 2) hp의 찾은 IP주소를  addr의 sin_addr 로 복사
	bcopy(hp->h_addr, &addr.sin_addr, hp->h_length);

    // 3) addr의 port와 통신타입(IPv4) 설정
	addr.sin_port = htons(port);
	addr.sin_family = AF_INET;

    // 4) 소켓 생성
	sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	//setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (const char *)&on, sizeof(int));

	if(sock == -1){
		perror("setsockopt");
		exit(1);
	}
	
    // 5) connect
	if(connect(sock, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) == -1){
		perror("connect");
		exit(1);

	}
	return sock;
}
 
#define BUFFER_SIZE 1024
#define MAXDATASIZE 1024
#define TRUE 1
// mode for using 'validate_HTTP_request' function.
enum get_mode {
    URL = 1,
    Host,
    Port,
    Path,
};


// validate HTTP request message format. check if "Host" HTTP header
// exists in header lines.
// return: 1 on vaild msg, 0 on invalid msg
int
validate_HTTP_request(char *msg){
  char *HTTP_header = "Host:";
  char *ptr = strstr(msg, HTTP_header); 

  if (ptr != NULL) return 1;
  return 0;
}


// get proper information piece, according to mode
// >> mode: get host(1), get port number(2), get path(3)
// return: 1 on success, 0 on any failure
int
parsing_HTTP_request(char *msg, char *dest_buf, int mode){
  char *ptr = NULL;
  char *backup = NULL; // backup of ptr
  char cpy_msg[MAXDATASIZE]; 
  memset(cpy_msg, 0, MAXDATASIZE);
  memcpy(cpy_msg, msg, strlen(msg) + 1);
  
  switch (mode){
  case URL:
	ptr = strstr(cpy_msg, "GET");
	ptr = strtok(ptr, " ");
	ptr = strtok(NULL, " ");

	memcpy(dest_buf, ptr, strlen(ptr) + 1);
	dest_buf[strlen(ptr)] = '\0';
	break;

  case Host:
	ptr = strstr(cpy_msg, "Host");
	ptr = strtok(ptr, " ");
	ptr = strtok(NULL, " ");
	strtok(NULL, "\r\n");

	// strip CRLF at end
	backup = ptr;
	while (*ptr != 13){ // 13: CR
		ptr++;
	}
	*ptr = '\0';
	memcpy(dest_buf, backup, strlen(backup) + 1);
    break;

  case Port:
  	ptr = strstr(cpy_msg, "GET");
	ptr = strtok(ptr, " ");
	ptr = strtok(NULL, " ");

	char *protocol = strstr(ptr,"://"); // position of protocol sign
	if (protocol != NULL)
		ptr = protocol+3;
	char *port = strchr(ptr, ':');
	if (port == NULL){ // port doesn't exist=>use 80
		char *port_80 = "80";
		memcpy(dest_buf, port_80, strlen(port_80) + 1);
		dest_buf[strlen(port_80)] = '\0';
		break;
	}

	port++;

	port = strtok(port, "/");
	memcpy(dest_buf, port, strlen(port) + 1);
	dest_buf[strlen(port)] = '\0';
	break;

  case Path:
    break;

  default:
    break;
  }


  return 1;
}


/*
 * find control sequence from each line
 * return flag:
 * flag = -1 => only single 'ENTER' is in buffer
 * flag = 1 => buffer is end with 'ENTER'
 * flag = 2 => now become 'ENTER' twice
 * flag = 0 => buffer isn't end with 'ENTER'
 */ 
int find_control_seq(char *buf, int was_ENT)
{
    assert(buf != NULL);
    if ((was_ENT == 1 || was_ENT == -1)&& buf[1] == '\n') // 'ENTER' twice ?
        return 2;


    size_t buf_len = strlen(buf);
    assert(buf_len > 0);
    char last_char = buf[buf_len-1];

    if (last_char == '\n'){
        if (buf_len == 1) return -1;
        else return 1;
    }

    return 0;
}

/*
 * read HTTP request message from client
 * returns total read bytes from client
 */
int message_handler(int sockfd, char *buf)
{
	int total_bytes = 0;
    int is_ENT = 0; // if 'ENTER' is at end of a line.

    memset(buf, 0, MAXDATASIZE);
    while (TRUE){
        // get a line from stdin
		total_bytes = total_bytes + read(sockfd, buf, MAXDATASIZE);
		printf(">> [%d]\n", total_bytes);
		if (total_bytes == -1) break;
        
        is_ENT = find_control_seq(buf, is_ENT);
		printf("is_ENT: %d\n", is_ENT);
        if (is_ENT == 2) // if control sequence, exit.
            return 1;
        else if (is_ENT == -1) // if single ENTER, continue.
            continue;

    }

    return total_bytes;
}


int main(int argc, char *argv[]){
	char *buf = "GET :1997/ HTTP/1.1\r\n"
            "Host: ee.kaist.ac.kr\r\n"
            "Connection: keep-alive\r\n"
            "Upgrade-Insecure-Requests: 1\r\n"
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n"
            "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/604.5.6 (KHTML, like Gecko) Version/11.0.3 Safari/604.5.6\r\n"
            "Accept-Language: en-us\r\n"
            "DNT: 1\r\n"
            "Accept-Encoding: gzip, deflate\r\n"
            "\r\n";

	int total_bytes = 0;
    int is_ENT = 0; // if 'ENTER' is at end of a line.
	char rec_buf[MAXDATASIZE];

    memset(rec_buf, 0, MAXDATASIZE);
    while (TRUE){
        // get a line from stdin
		total_bytes = total_bytes + read(0, rec_buf, MAXDATASIZE);
		if (total_bytes == -1) break;
        
        is_ENT = find_control_seq(rec_buf, is_ENT);
		printf("received byte: [%d]  is_ENT: %d\n", total_bytes, is_ENT);
        if (is_ENT == 2) // if control sequence, exit.
            return 1;
        else if (is_ENT == -1) // if single ENTER, continue.
            continue;

    }



	/*
	int is_valid = validate_HTTP_request(buf);
	char get_URL[MAXDATASIZE];
	char get_host[MAXDATASIZE];
	char get_port[MAXDATASIZE];
	char get_path[MAXDATASIZE];
	memset(get_URL, 0, MAXDATASIZE);
	memset(get_host, 0, MAXDATASIZE);
	memset(get_port, 0, MAXDATASIZE);
	memset(get_path, 0, MAXDATASIZE);

	parsing_HTTP_request(buf, get_URL, URL);
	parsing_HTTP_request(buf, get_host, Host);
	parsing_HTTP_request(buf, get_port, Port);
	parsing_HTTP_request(buf, get_path, Path);
	

	printf("\n\n---------------------\n");
	printf("URL[%zu]: %s\n", strlen(get_URL), get_URL);
	printf("Host[%zu]: %s\n", strlen(get_host),get_host);
	printf("Port[%zu]: %s\n", strlen(get_port),get_port);
	printf("Path[%zu]: %s\n", strlen(get_path),get_path);
	printf("---------------------\n\n");
	*/

	return 0;
}

/*
int main(int argc, char *argv[]){
	int fd;
	char buffer[BUFFER_SIZE];

	if(argc < 3){
		fprintf(stderr, "Usage: %s <hostname> <port>\n", argv[0]);
		exit(1); 
	}
       
	fd = socket_connect(argv[1], atoi(argv[2])); 
	write(fd, "GET /\r\n", strlen("GET /\r\n")); // write(fd, char[]*, len);  
	bzero(buffer, BUFFER_SIZE);
	
	while(read(fd, buffer, BUFFER_SIZE - 1) != 0){
		fprintf(stderr, "%s", buffer);
		bzero(buffer, BUFFER_SIZE);
	}

	shutdown(fd, SHUT_RDWR); 
	close(fd); 

	return 0;
}
*/